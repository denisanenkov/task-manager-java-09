package ru.anenkov.tm.api;

import ru.anenkov.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    String[] getCommands();

    String[] getArgs();

    TerminalCommand[] getTerminalCommands();

}
