package ru.anenkov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "Help";

    String CMD_VERSION = "Version";

    String CMD_ABOUT = "About";

    String CMD_EXIT = "Exit";

    String CMD_INFO = "Info";

    String CMD_ARGUMENTS = "Arguments";

    String CMD_COMMANDS = "Commands";

}
