package ru.anenkov.tm;

import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.model.TerminalCommand;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    public static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
        }
    }

    private static void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case (TerminalConst.CMD_HELP):
                showHelp();
                break;
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_INFO:
                showInfo();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
        }
    }

    public static boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    private static void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Denis Anenkov");
        System.out.println("E-MAIL: denk.an@inbox.ru");
    }

    private static void showCommands() {
        final String[] commands = COMMAND_REPOSITORY.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    private static void showArguments() {
        final String[] arguments = COMMAND_REPOSITORY.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

}
